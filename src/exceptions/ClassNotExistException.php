<?php

namespace Pantagruel74\CollectionGenerator\exceptions;

class ClassNotExistException extends \RuntimeException
{
    protected $message = 'Class does not exist';
}