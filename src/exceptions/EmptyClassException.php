<?php

namespace Pantagruel74\CollectionGenerator\exceptions;

class EmptyClassException extends \ErrorException
{
    protected $message = 'Classname is empty';
}