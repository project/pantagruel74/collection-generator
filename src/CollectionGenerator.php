<?php

namespace Pantagruel74\CollectionGenerator;

use Pantagruel74\CollectionGenerator\tools\CollectionParser;

class CollectionGenerator
{
    /**
     * @throws exceptions\EmptyClassException
     */
    public function generateForClass(string $class): void
    {
        $parser = new CollectionParser($class);
        $parser->generateCollection();
    }
}