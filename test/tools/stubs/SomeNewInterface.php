<?php

namespace Pantagruel74\CollectionGeneratorTest\tools\stubs;

interface SomeNewInterface
{
    public function getVal(): string;
    public function setVal(string $val): void;
}